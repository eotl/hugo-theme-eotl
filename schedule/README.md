Hugo Module: Schedule
=====================

1. Install the module

```
hugo mod get codeberg.org/eotl/hugo-mods/schedule
```

2. Add the following to your `hugo.yaml` config file in your `module:` section

```
module:
  imports:
    - path: "codeberg.org/eotl/hugo-mods/schedule"
      mounts:
        - source: "layouts/calendar"
          target: "layouts/events"
```

Unless your schedule files are stored in `content/calendar/` you also need to
mount the `source:` into the correct `target:` path name which matches. The
snippet above expects your files to exist in `content/events/` path.

3. Now add the various output formats of `Calendar` to the following:

```
mediaTypes:
  text/calendar:
    suffixes:
      - "ics"

outputs:
  section: ["HTML", "RSS", "Calendar"]
  page: ["HTML", "Calendar"]

outputFormats:
  Calender:
    protocol: "https://"
    mediaType: "text/calendar"
    baseName: "schedule"
    isPlainText: true
```

4. Voila. It should work now on build

- http://localhost:1313/events
