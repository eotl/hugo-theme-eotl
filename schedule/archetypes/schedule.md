---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: {{ .Date }}
time_start: 18:00:00
time_end: 22:00:00
tags: ["Meeting", "Food", "Presentation"]
---
