Hugo Module: Debug
==================

A partial which prints every Hugo variable in an easy to reference view at the
bottom of the pages of your Hugo site.

---

## Use

Install module

```
$ hugo mod codeberg.org/basebuilder/hugo-mods/debug
```

Add the following config variable to your `hugo.yaml` file

```
params:
  debugging: true
```

```
{{ partial "debug.html" . }}
```
