Hugo Module: Edited
===================

A [Hugo Module](https://gohugo.io/hugo-modules/use-modules/) which renders info
from `.GitInfo` and `.CodeOwners` as simple partial widget to be placed in a
footer or sidebar.

## Use Module

Install module:

```
hugo mod get codeberg.org/eotl/hugo-mods/edited
```

Add the followings bit to your `hugo.yaml` config file:

```
module:
  imports:
    - path: "codeberg.org/eotl/hugo-mods/edited"
```

If you want to provide link to the git repo where the code is hosted, add the
following value:

```
params:
  repo: "http://your-git-server.com/org/repo"
```

Then include the following `partial` in your layout files:

```
{{ partial "edited.html" . }}
```

## Styling

As you can see by inspecting the `edited.html` partial, there is CSS classes on
a wrapper div:

```
<div class="hugo-module edited">
  ...
```

Add whatever custom styling to those classes in your site's CSS.
