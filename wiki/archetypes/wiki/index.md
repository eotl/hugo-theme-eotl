---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: {{ .Date }}
date_created: {{ .Date }}
draft: true
type: "wiki"
leafs: []
branches: ""
sections: [] 
links: []
items: []
places: []
---
