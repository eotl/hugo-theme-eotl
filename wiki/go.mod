module codeberg.org/eotl/hugo-mods/wiki

go 1.19

require (
	codeberg.org/eotl/hugo-mods/edited v0.0.0-20231220100816-7cc3321f87cf // indirect
	codeberg.org/eotl/bootstrap-eotl v0.0.0-20231108072808-054ee7d83d18 // indirect
	codeberg.org/eotl/icons v0.0.0-20231107155203-94940c5c2fdf // indirect
	github.com/gohugoio/hugo-mod-bootstrap-scss-v4 v1.6.1 // indirect
)
