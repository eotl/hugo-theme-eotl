#!/bin/bash

echo "What would you like to call your instance?"
read TITLE
if [ -z "$TITLE" ]; then
	echo "$0: error, empty title" >&2
	exit 1
fi

ID=$(echo "$TITLE" | sed "s/ /-/" | awk '{print tolower($0)}')
echo -e "The following will be your instance URL:\n"
echo -e "$ID\n"

echo "Write a short sentence about your instance:"
read DESCRIPTION
if [ -z "$DESCRIPTION" ]; then
	echo "$0: error, empty description" >&2
	exit 1
fi

echo "Who can access this instance?"
echo "1) Public"
echo "2) Private"
read ACCESS

if [[ "$ACCESS" != "1" && "$ACCESS" != "2" ]]; then
	echo "$0: error, unavailable access choice" >&2
	exit 1
fi

echo "Should this instance be listed on the homepage?"
echo "y) yes"
echo "n) no"
read LISTED

if [[ "$LISTED" != "y" && "$LISTED" != "n" ]]; then
	echo "$0: error, unavailable listing choice" >&2
	exit 1
fi

echo "Choose a theme:"
echo "w) white on black background"
echo "b) black on white background"
read THEME

if (( "$THEME" < "w" || "$THEME" >  "b" )); then
	echo "$0: error, unavailable theme choice!" >&2
	exit 1
fi

echo "Choose a style of product images:"
echo "e) eotl"
echo "s) simple"
read IMAGES

if (( "$IMAGES" < "e" || "$IMAGES" >  "s" )); then
	echo "$0: error, unavailable item images choice!" >&2
	exit 1
fi

OUTPUT="\
---\n
id: \"${ID}\"\n
title: \"${TITLE}\"\n
description: \"${DESCRIPTION}\"\n
city: \"${CITY}\"\n
country: \"${COUNTRY}\"\n
zone: \"\"\n
neighborhood: \"\"\n
listed: \"${LISTED}\"\n
access: \"${ACCESS}\"\n
style: \"${THEME}\"\n
style_items: \"${IMAGES}\"\n
permalink: "/${TITLE}/"\n
---
"

SAVE_FILE="${ID}.md"
echo -e $OUTPUT > $SAVE_FILE

echo -e "You created an instance with the following settings:\n"
echo -e $OUTPUT
echo -e "\n"
echo -e "Your instance was created in:\n"
echo -e "\t$SAVE_FILE\n"
