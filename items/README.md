Open Items Site
===============

A template for making simple websites with EOTL's [ui-ordering.js](/eotl/ui-ordering) and [Hugo](https://gohugo.io) which generates a single or category paged item websites. Rudimentary suport for data from:

- [Open Inventory](https://inventory.eotl.supply)
- [CoopCycle](https://coopcycle.org)

When using **Open Inventory** ordering can be connected to an 
[Open Dispatch](https://dispatch.eotl.supply) back-end.


## Configuring

To configure edit the `config.yaml` file which has all the normal Hugo config
variables, but also the following custom values stored in `params:`


**Content & Style**

```
params:
  style: "eotl"
  styleValue: "light-mode"
  logo: "simple-food-logo.svg"
  sections:
    - welcome
    - offerings
    - gallery
    - bottom
```

**Open Inventory & Dispatch**

To interact with EOTL's stack edit with the following values:

```
  inventoryApi: "open-inventory"
  inventoryUrl: "https://inventory.eotl.supply/api/suppliers/free-open-kitchen"
  dispatchApi: "open-dispatch"
  dispatchUrl: "https://dispatch.eotl.supply"
  dispatchCarrier: "courier-kollektiv"
```


**CoopCycle** 

To use data pulled from a [CoopCycle](https://coopcycle.org) instance:

```
  inventoryApi: "coopcycle"
  inventoryUrl: "/api/menu.json"
  dispatchApi: "coopcycle"
  dispatchUrl: "https://khora.berlin/en/restaurant/149-oh-la-queca"
  dispatchCarrier: "khora"
```

*Let's get off this runaway train*
