/*!
  * eotl.js - https://eotl.network
  * Copyright 2020-2021 EOTL Authors (https://codeberg.org/eotl/theme-bootstrap)
  * Licensed under ISC (https://codeberg.org/eotl/theme-bootstrap/raw/branch/main/LICENSE)
  */

function longNowify(elements) {
    let itemDate = '';
    for (element in elements) {
        itemDate = elements[element].innerHTML
        if (itemDate) {
            var year = itemDate.match(/(?:01|20)\d{2}/g);
            var longYear = '0' + year[0]
            var fixed = itemDate.replace(year[0], longYear)
            elements[element].innerHTML = fixed
            elements[element].setAttribute("title", "Extra '0' to question: how long is now?")
        }
    }
}
/* meaning.js - explains some of what EOTL means */
var meanings = {
    "one": {
        "headline": "End Of The Line",
        "url": "https://eotl.supply"
    },
    "two": {
        "headline": "Excited Other Techniques Living",
        "url": "https://en.wikipedia.org/wiki/Freight_bicycle"
    },
    "three": {
        "headline": "Elegant Options Towards Life",
        "url": "https://original-unverpackt.de"
    },
    "four": {
        "headline": "Ecological Obstacles Terrorizing Life",
        "url": "https://public.wmo.int/en/media/press-release/wmo-climate-statement-past-4-years-warmest-record"
    },
    "five": {
        "headline": "Ecocide On The Land",
        "url": "https://www.theguardian.com/commentisfree/2019/mar/15/climate-strikers-urgency-un-summit-world-leaders"
    },
    "six": {
        "headline": "Extinquishing Other Terains Longevity",
        "url": "https://www.nature.com/articles/s41467-018-05252-y"
    },
    "seven": {
        "headline": "Ending Of Tranquil Life",
        "url": "https://www.nature.com/articles/d41586-019-02782-3"
    },
    "eight": {
        "headline": "Eat Off The Land",
        "url": "https://markthalleneun.de"
    },
    "nine": {
        "headline": "Extinction Of The Living",
        "url": "https://climatepeopleorg.com/2016/05/04/the-climate-crisis-in-quotations/"
    },
    "ten": {
        "headline": "Empowering Open Technological Liberation",
        "url": "https://codeberg.org/eotl"
    },
    "eleven": {
        "headline": "Electricity Obtained To Light",
        "url": "https://eotl.solar"
    },
    "tweleve": {
        "headline": "Ecstatic Objects That Live",
        "url": "https://morningchores.com/mushroom-farming/"
    },
    "thirteen": {
        "headline": "Egalitarian Open Together Life",
        "url": "https://www.aardehuis.nl"
    },
    "fourteen": {
        "headline": "Explore Outwardly To Links",
        "url": "https://eotl.links"
    },
    "fifteen": {
        "headline": "Elevate Orientate Towards Liberation",
        "url": "https://www.ted.com/talks/ron_finley_a_guerrilla_gardener_in_south_central_la"
    },
    "sixteen": {
        "headline": "Emerge Organize Towards Luminesscence",
        "url": "https://solargis.info/imaps/"
    },
    "seventeen": {
        "headline": "Global plastic waste on track to triple by 2060",
        "url": "https://www.france24.com/en/live-news/20220603-global-plastic-waste-on-track-to-triple-by-2060"
    }
}

var meaningArray = Object.keys(meanings)

function meaningWhat() {
    var position = meaningArray[meaningArray.length-1]
    var tickerContainer = $(".meaning-cycle")
    var meaningText = $(".meaning-text")

    tickerContainer.fadeIn(1000, function() {
        $(this).delay(4500).fadeOut('fast', meaningWhat)
    })
    meaningText.html('&nbsp;')

    var piece = meanings[position].headline.split(' ')

    setTimeout(function() {
        meaningText.append(piece[0])
    }, 1000)

    setTimeout(function() {
        meaningText.append(' ' + piece[1])
    }, 1500)

    setTimeout(function() {
        meaningText.append(' ' + piece[2])
    }, 2000)
    setTimeout(function() {
        meaningText.append(' ' + piece[3])
    }, 2500)

    meaningText.wrap("<a class='meaning-link' href='" + meanings[position].url + "' target='_blank'></a>")
    meaningArray.unshift(position)
    meaningArray.pop()
}
/* switcher.js - toggles between light and dark modes */

function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}

// Toggle between modes
function toggleTheme() {
    var btnSwitch = document.getElementById('switch')
    if (localStorage.getItem('theme') === 'light-mode') {
        setTheme('dark-mode')
        var newHTML = `
            <img class="switch-img" src="/images/theme-dark.svg" title="switch theme" width="auto" height="14">
            <span>Light</span>`
    } else {
        setTheme('light-mode')
        var newHTML = `
            <img class="switch-img" src="/images/theme-light.svg" title="switch theme" width="auto" height="14">
            <span>Dark</span>`
    }
    btnSwitch.innerHTML = newHTML
}

// Set theme on load dark-mode by default
document.addEventListener('DOMContentLoaded', function() {
    (function() {
        var btnSwitch = document.getElementById('switch')
        if (localStorage.getItem('theme') === 'light-mode') {
            setTheme('light-mode')
            var newHTML = `
                <img class="switch-img" src="/images/theme-dark.svg" title="switch theme" width="auto" height="14">
                <span>Dark</span>`
        } else {
            setTheme('dark-mode')
            var newHTML = `
                <img class="switch-img" src="/images/theme-light.svg" title="switch theme" width="auto" height="14">
                <span>Light</span>`
        }
        if (btnSwitch != null && btnSwitch.innerHTML != "") {
            btnSwitch.innerHTML = newHTML;
        }
    })()
})
/* showLeafs() - based on code taken from source https://micro.blog/about */
const colors = [ "#008000", "#00b000", "#76a147", "#9af092", "#cacd4c", "#6ec100", "#258341", "#5a6429", "#be9132", "#b6975f" ];

function hexToRGBA(hex, opacity) {
    hex = hex.replace(/^#/, ""); // remove "#" if present
    var r = parseInt(hex.substring(0, 2), 16);
    var g = parseInt(hex.substring(2, 4), 16);
    var b = parseInt(hex.substring(4, 6), 16);
    return "rgba(" + r + "," + g + "," + b + "," + opacity + ")";
}
    
function showLeafs() {
    // hide the button and show features
    document.getElementById("feature_showmore").style.display = "none";
    document.getElementById("feature_bubbles").style.display = "block";
    
    // get all spans with ids 1 to 87
    var spans = [];
    for (var i = 1; i <= 87; i++) {
        spans.push(document.getElementById(i.toString()));
    }
f
    // make first item visible right away
    var first_item = spans[0];
    first_item.style.opacity = 1;

    // loop over each span
    for (var i = 0; i < spans.length; i++) {
        (function(i) {
            var span = spans[i];
            var random_color = colors[i % colors.length]; // cycle through colors if there are more spans than colors

            // set color right away, half opacity
            span.style.backgroundColor = hexToRGBA(random_color, 0.5);

            // random delay between 0 and 3 seconds
            var random_delay = Math.random() * 3000;

            // set a timeout to change the background color after the random delay
            setTimeout(function() {
                span.style.opacity = 1;
            }, random_delay);
        })(i);
    }
}
