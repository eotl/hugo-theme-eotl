---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
draft: true
date: '{{ .Date.Format "2006" }}'
lang: "English"
url: "/articles/{{ .File.ContentBaseName }}"
creators: []
leafs: []
---
