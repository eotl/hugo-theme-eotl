---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
draft: true
description: "Brief description of the project"
image: ""
status: "proposed"
milestones:
creators: ["eotl"]
type: "project"
period: [ "2020" ]
urls:
language: "English"
permalink: ""
style: ""
leafs: []
branch: "network"
roots: ""
details: []
---
