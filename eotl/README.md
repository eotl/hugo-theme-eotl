EOTL Hugo Theme
===============

A high-contrast theme for Hugo static site framework created by EOTL that
defaults to dark-mode.

- [Learn about EOTL](https://eotl.network)

### Useing

In your website project repo 

```
$ hugo mod get codeberg.org/eotl/hugo-mods/eotl
```

Create the file `layouts/_default/baseof.html` and fill it with the following:

```
<!DOCTYPE html>
<html lang="{{ site.LanguageCode | default "en-us" }}" class="no-ie no-js {{ $.Site.Params.ui.theme }}">
    <head>
        {{- partial "head.html" . -}}
    </head>
    <body class="px-2">
        {{- partial "header.html" . -}}
        {{- block "main" . }}{{- end }}
        {{ partial "footer.html" . }}
        {{- partial "javascripts.html" . -}}
	      {{ if $.Site.Params.debugging }}{{- partial "debug.html" . -}}{{ end }}
    </body>
</html>
```

*Note: it is needed this file lives outside of the theme so the site can load both `eotl` and `debug` modules*


### Credits

Included or utilized projects and code snippets:

- [Hugo](https://gohugo.io)
- [Bootstrap 4](https://getbootstrap.com)
- [Docsy](https://www.docsy.dev)
- [Reveal.js](https://revealjs.com)
