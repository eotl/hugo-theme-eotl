Hugo Modules
============

A collection of various things created for [Hugo](https://gohugo.io) static site
generator. All widgets can be used in Hugo sites, but come with classes for
[Bootstrap 4](https://getbootstrap.com) embedded.

## Partials

### Debug

Prints all Hugo variables `.Site` and `.Page` as well as from Template and Section.

### Edited

Display information of who last edited a page via Git information.

---

## Layouts

Full layouts which generate List and Single pages, along with partials to include
in main theme.

### Items

Renders basic product category and item pages for shopping or collective style
use.

### Neighbors

For small pods or collectives which independently coordinate sharing of
resources, like food coops.

### Recently

Shows recently edited pages in a list, as well as widgets to include elsewhere.
Useful for wiki and knowledgebase style sites.

### Schedule

Basic calendaring which displays schedule overview and individual
event pages. Also renders feed and event specific `.ics` data.

### Wiki

A Hugo-esque riff on classic Wiki architecture of floating pages.

---

## Themes

Full themes to base your Hugo site on.

### EOTL

An extinction related Hugo Theme which incorporates our custom [Bootstrap 4)(https://eotl.codeberg.page/bootstrap-eotl/) 
theme used across apps.
